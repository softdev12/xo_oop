/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nattiya.xo_oop;

import java.util.Scanner;

/**
 *
 * @author DELL
 */
public class Game {
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row,col;
    char ans;
    Scanner kb = new Scanner(System.in);

    public Game(){
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome(){
        System.out.println("Welcome to XO Game");
    }
    public void showBye() {
        System.out.println("Bye bye ...");
    }

    public void showTable(){
        table.showTable();
    }

    public void input(){
        while (true) {
            System.out.println("please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if(table.setRowCol(row,col)){
                break;
            }
            System.out.println("Error: table at row and col is not empty!!");
        }
    }

    public void showTurn(){
        System.out.println(table.getCurrentPlayer().getName()+" turn");
    }
    public void newGame(){
        table = new Table(playerX,playerO);
    }

    public void run(){
        this.showWelcome();
        while(true){
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (checkFinish()) {
                break;
            }

            table.switchPlayer();
        }
        this.showBye();
    }
private boolean checkFinish() {
        if (table.isFinish()) {
            this.showTable();
            if (table.getWinner() == null) {
                System.out.println("Draw!!");
            } else {
                System.out.println(table.getWinner().getName() + " Win!!");
            }
            if (checkNewGame()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkNewGame() {
        System.out.println("new game?(y,n)");
        ans = kb.next().charAt(0);
        if ('y'==ans) {
            newGame();
        } else if ('n'==ans) {
            return true;
        }
        return false;
    }

}
